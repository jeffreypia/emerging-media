<?php
/**
 * The loop that displays posts.
 */
?>

<section class="posts-listing">

	<?php /* If there are no posts to display, such as an empty archive page */ ?>
	<?php if ( have_posts() ) : ?>

		<ul class="posts">
			<?php
				/* Start the Loop.
				 *
				 * We use the same loop in multiple contexts.
				 * It is broken into three main parts: when we're displaying
				 * posts that are in the gallery category, when we're displaying
				 * posts in the asides category, and finally all other posts.
				 *
				 * Additionally, we sometimes check for whether we are on an
				 * archive page, a search page, etc., allowing for small differences
				 * in the loop on each template without actually duplicating
				 * the rest of the loop that is shared.
				 *
				 * Without further ado, the loop:
				 */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<li <?php post_class() ?>>
					<h3 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

					<article class="post-excerpt">
						<?php the_excerpt(); ?>
					</article>

					<div class="post-meta">
						<span class="post-date">Posted on <?php echo get_the_date( 'F j, Y' ); ?></span>
						<span class="post-comment-count disqus-comment-count" data-disqus-url="<?php the_permalink(); ?>"><?php echo $post->comment_count; ?> comments</span>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
					
		<nav id="nav-below" class="navigation button-container">
			<?php echo paginate_links(); ?>
		</nav><!-- #nav-below -->

	<?php else : ?>
		<div id="post-0" class="post error404 not-found">
			<h3 class="post-title"><?php _e( 'Not Found', 'boilerplate' ); ?></h3>
			
			<article class="post-excerpt">
				<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'boilerplate' ); ?></p>
			</article><!-- .entry-content -->
		</div>

	<?php endif; ?>
</section>
