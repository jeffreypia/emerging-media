<?php
/**
 * The Sidebar on the Blog page
 */
?>

<?php get_template_part( 'partial', 'category-filter' ); ?>

<?php get_search_form(); ?>

<?php get_template_part( 'partial', 'icontact' ); ?>

<section class="posts-listing">

<?php
	// This loops pulls the latest 6 posts.
	// Used query_posts due to requirement for pagination
	$latestPosts = new WP_Query('posts_per_page=' . get_option('posts_per_page') . '&post_type=post');
	$temp_query = $wp_query;
	$wp_query = NULL;
	$wp_query = $latestPosts;
	if ( $latestPosts->have_posts() ) :
?>
		<ul class="posts">
<?php 
			 while ( $latestPosts->have_posts() ) :
				$latestPosts->the_post();
?>

				<li <?php post_class() ?>>
					<h3 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

					<article class="post-excerpt">
						<?php the_excerpt(); ?>
					</article>

					<div class="post-meta">
						<span class="post-date">Posted on <?php echo get_the_date( 'F j, Y' ); ?></span>
						<span class="post-comment-count disqus-comment-count" data-disqus-url="<?php the_permalink(); ?>"><?php echo $post->comment_count; ?> comments</span>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>

		<nav id="nav-below" class="navigation button-container">
			<span class="page-numbers current">1</span>
<?php
			$maxPages = $wp_query->max_num_pages;
			if( $maxPages > 1 ) :
				for ($i = 2; $i <= $maxPages; $i++) :
?>
					<a class="page-numbers" href="<?php echo get_permalink( get_option('page_for_posts' ) ) . 'page/' . $i; ?>/"><?php echo $i; ?></a>
				<?php endfor; ?>
				<a class="next page-numbers" href="<?php echo get_permalink( get_option('page_for_posts' ) ) . 'page/2/' ?>">Next »</a>
			<?php endif; ?>

		</nav><!-- #nav-below -->
<?php
	endif;
	$wp_query = NULL;
	$wp_query = $temp_query;
	wp_reset_query();
?>