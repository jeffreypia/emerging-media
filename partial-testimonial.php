<?php $bg = get_field('testimonial_background'); ?>
<section class="testimonial">
	<img src="<?php echo $bg['url']; ?>" class="testimonial-background" alt="">
	<p class="testmonial-quote">
		<?php echo get_field('testimonial_copy') ?>
		<strong class="testmonial-attribution">– <?php echo get_field('testimonial_attribution') ?></strong>
	</p>
</section>
