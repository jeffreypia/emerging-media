<?php $img = get_field('our_work_section_image'); ?>
<section class="our-work">
	<div class="container">
		<img class="our-work-bg" src="<?php echo $img['url']; ?>">
		<h2 class="section-title"><?php echo get_field('our_work_section_title'); ?></h2>
		<article class="section-copy"><?php echo get_field('our_work_section_copy'); ?></article>
	</div>
</section>