<?php $bg = get_field('banner_image'); ?>
<section class="banner home-banner" <?php if( $bg ) { echo 'style="background-image: url('.$bg['url'].');"'; } ?>>
	<div class="container">
		<h1 class="section-title">
			<?php echo get_field('banner_title'); ?>
			<?php echo get_field('banner_subtitle'); ?>
		</h1>
	</div>
</section>
