<?php if( have_rows('journey_bullet') ): ?>
<section class="journey">

	<h3 class="section-title"><a href="<?php echo get_field('journey_link'); ?>"><?php echo get_field('journey_section_title'); ?></a></h3>
	<article class="section-copy">
		<p><?php echo get_field('journey_copy'); ?></p>
	</article>

	<ul class="bullets">
		<?php while ( have_rows('journey_bullet') ) : the_row(); ?>
			<li class="bullet">
				<?php $icon = get_sub_field('icon'); ?>
				<img class="bullet-icon" src="<?php echo $icon['url']; ?>" alt="">
				<h3 class="bullet-title"><?php the_sub_field('title'); ?></h3>
				<div class="bullet-content">
					<?php the_sub_field('copy'); ?>
				</div>
			</li>
		<?php endwhile; ?>
	</ul>

</section>
<?php endif; ?>