<section class="hireology">
	<div class="container">
		<h2 class="section-title"><?php echo get_field('hireology_title'); ?></h2>
		<?php echo get_field('hireology'); ?>
	</div>
</section>