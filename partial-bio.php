<?php $bg = get_field('bio_image'); ?>
<section class="bio">
	<div class="container">
		<?php if( $bg ) { echo '<img class="bio-image" src="'.$bg['url'].'" alt="'.get_field('bio_name').'">'; } ?>
		<h2 class="bio-name"><?php echo get_field('bio_name'); ?></h2>
		<div class="bio-title"><?php echo get_field('bio_title_1'); ?></div>
		<div class="bio-title"><?php echo get_field('bio_title_2'); ?></div>
		<article class="section-copy"><?php echo get_field('bio_copy'); ?></article>
	</div>
</section>