<?php if( have_rows('award') ): ?>
<section class="slider awards">
	<h3 class="section-title"><?php echo get_field('awards_section_title'); ?></h3>

	<div class="awards-list">
		<ul class="slides">
			<?php while ( have_rows('award') ) : the_row(); ?>
				<li class="award slide">
					<?php $image = get_sub_field('image'); ?>
					<img class="award-image" src="<?php echo $image['url']; ?>">
					<img class="award-image" src="<?php echo $image['url']; ?>">
				</li>
			<?php endwhile; ?>
		</ul>
	</div><!-- .awards-list -->
</section>
<?php endif; ?>