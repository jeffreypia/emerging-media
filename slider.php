<?php
if( have_rows('slides') ):
	$slides = get_field('slides');
?>
	<section class="slider slider-main">
		<ul class="slides">
<?php
			foreach( $slides as $slide):
				$post = $slide[slide];
				setup_postdata($post);
?>
				 <?php get_template_part( 'slide', get_field('slide_type') ); ?>
<?php
			endforeach;
			wp_reset_postdata();
?>
		</ul>
	</section>
<?php endif; ?>