<?php if( have_rows('journey_bullet') ): ?>
<section class="values">
	<div class="container">
		<h2 class="section-title"><?php echo get_field('journey_section_title'); ?></h2>
		<article class="section-copy">
			<p><?php echo get_field('journey_copy'); ?></p>
		</article>
	</div>

	<ul class="bullets">
		<?php while ( have_rows('journey_bullet') ) : the_row(); ?>
			<li class="bullet">
				<h3 class="bullet-title"><?php echo the_sub_field('title'); ?></h3>
			</li>
		<?php endwhile; ?>
	</ul>
</section>
<?php endif; ?>