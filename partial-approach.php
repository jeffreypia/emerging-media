
<section class="approach">
	<ul class="bullets">
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-idea.png">
			<h3 class="bullet-title">Idea</h3>
			<div class="bullet-content">
				<ol>
					<li>Giant opportunity</li>
					<li>Painful problem</li>
					<li>Desire to change the world, for the better</li>
				</ol>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-build.png">
			<h3 class="bullet-title">Build</h3>
			<div class="bullet-content">
				<ol>
					<li>Build a product</li>
					<li>Build a team</li>
				</ol>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-fund.png">
			<h3 class="bullet-title">Fund</h3>
			<div class="bullet-content">
				<ol>
					<li>Raise $</li>
					<li>Bootstrap</li>
				</ol>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-compete.png">
			<h3 class="bullet-title">Compete</h3>
			<div class="bullet-content">
				<ul>
					<li>Advisors</li>
					<li>Partners</li>
				</ul>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-partner.png">
			<h3 class="bullet-title">Partner</h3>
			<div class="bullet-content">
				<ul>
					<li>Startups</li>
					<li>Goliath(s)</li>
					<li>Regulators</li>
					<li>Nay sayers</li>
				</ul>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-grow.png">
			<h3 class="bullet-title">Grow</h3>
			<div class="bullet-content">
				<ul>
					<li>Customers</li>
					<li>Advocates</li>
					<li>Fans &amp; Followers</li>
					<li>Revenue</li>
					<li>Lives Touched</li>
				</ul>
			</div>
		</li>
		<li class="bullet">
			<img class="bullet-icon" src="/wp-content/uploads/icon-finish.png">
			<h3 class="bullet-title">Finish Line</h3>
			<div class="bullet-content">
				<ul>
					<li>Massive growth</li>
					<li>Acquisition/IPO</li>
					<li>Happy customers</li>
					<li>Lives Changed</li>
					<li>Global influence</li>
				</ul>
			</div>
		</li>
	</ul>

	<h3 class="section-title">
		You disrupt. We connect.
		<strong>The <em>Connect4<sup>TM</sup></em> Approach</strong>
	</h3>
	<article class="section-copy">
		<p>A proper brand foundation, lead-generating marketing, awareness building PR and an engaged social community. When the four align, the vision, the passion and the story that fueled an idea will become the road-map to your disruptor journey.</p>

		<div class="button-container">
			<a class="button" href="/how-we-do-it/">Read More</a>
		</div>
	</article>

</section>

