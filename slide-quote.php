<li class="slide slide-quote slide-<?php echo $post->post_name; ?>">
	<div class="container">
		<article class="slide-content">
			<?php the_content(); ?>

			<footer class="quote-footer">
				<strong class="quote-attribution">– <?php echo get_field('attribution'); ?></strong>
			</footer>
		</article>
	</div>
</li>
