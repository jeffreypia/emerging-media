<?php if( have_rows('person') ): ?>
<section id="people" class="people">
	<h3 class="section-title"><?php echo get_field('people_section_title'); ?></h3>
	<ul>
		<?php while ( have_rows('person') ) : the_row(); ?>
			<?php $img = get_sub_field('person_thumbnail'); ?>
			<li class="person">
				<img class="person-thumbnail" src="<?php echo $img['url']; ?>">
				<div class="person-details">
					<h3 class="person-name"><?php the_sub_field('person_name'); ?></h3>
					<?php if( get_sub_field('person_title') ) : ?>
						<div class="person-title"><?php the_sub_field('person_title'); ?></div>
					<?php endif; ?>
					<?php the_sub_field('person_copy'); ?>
				</div>
			</li>
		<?php endwhile; ?>
	</ul>
</section>
<?php endif; ?>