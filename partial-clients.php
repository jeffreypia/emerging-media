<?php if(get_field('client')) : ?>
<section id="clients" class="clients">
	<h3 class="section-title">Our clients</h3>
	<ul class="container">
		<?php
			$i = 0;
			while(has_sub_field('client')) :
				$i++;
				$clientImage = get_sub_field('client_logo');
		?>
				<li class="client <?php if($i > 30) { echo  'hidden'; } ?>">
					<img src="<?php echo $clientImage['url']; ?>" alt="<?php the_sub_field('client_name'); ?>">
				</li>
		<?php endwhile; ?>
	</ul>
	<button class="see-more">See More</button>
</section>
<?php endif; ?>
