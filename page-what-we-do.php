<?php
/**
* The What We Do page template.
*/

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
	<?php get_template_part( 'banner', 'what-we-do' ); ?>

	<?php get_template_part( 'partial', 'testimonial' ); ?>

	<?php get_template_part( 'slider', 'what-we-do' ); ?>

	<?php get_template_part( 'partial', 'clients' ); ?>

	<?php get_template_part( 'partial', 'acquisitions' ); ?>

	<?php get_template_part( 'partial', 'contact' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>