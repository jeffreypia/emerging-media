<?php if( have_rows('connection') ): ?>
<section id="connections" class="connections">
	<div class="container">
		<h3 class="section-title"><?php echo get_field('connection_section_title'); ?></h3>
		<ul>
			<?php while ( have_rows('connection') ) : the_row(); ?>
				<?php $img = get_sub_field('connection_logo'); ?>
				<li class="connection">
					<a href="<?php the_sub_field('connection_link'); ?>" target="_blank" title="Link to <?php the_sub_field('connection_title'); ?>">
						<img src="<?php echo $img['url']; ?>" alt="<?php the_sub_field('connection_title'); ?>">
					</a>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
</section>
<?php endif; ?>