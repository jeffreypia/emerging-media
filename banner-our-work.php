<section class="banner our-work-banner">
	<div class="container">
		<h1 class="section-title"><?php echo get_field('banner_title'); ?></h1>
	</div>
</section>
