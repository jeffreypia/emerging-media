<?php
	$target = get_field('link');
	$category = get_the_category($target->ID); 
?>
<li class="slide slide-blog slide-<?php echo $post->post_name; ?>">
	<div class="container">
		<header class="slide-header">
			<div class="slide-category cat-<?php echo $category[0]->slug; ?>"><?php echo $category[0]->name; ?></div>
			<h3 class="slide-title"><?php the_title(); ?></h3>
		</header>

		<article class="slide-content">
			<?php the_content(); ?>
		</article>

		<div class="button-container">
			<a class="button" href="<?php echo get_permalink( $target->ID ); ?>" title="Link to <?php echo $target->post_title; ?>">Read More</a>
		</div><!-- .button-container -->
<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('full', array( 'class'	=> 'slide-bg'));
		} 
?>
	</div><!-- .container -->
</li>
