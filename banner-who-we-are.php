<section class="callout">
	<div class="container">
		<h1 class="section-title">
			<?php echo get_field('banner_title'); ?> <br>
			<strong><?php echo get_field('banner_subtitle'); ?></strong>
		</h1>
		<article class="section-copy"><?php echo get_field('banner_copy'); ?></article>
	</div>
</section>