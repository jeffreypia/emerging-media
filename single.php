<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

				<section class="banner blog-banner">
					<div class="container">
						<h1 class="section-title">Content that Matters</h1>
					</div>
				</section>

				<?php get_template_part( 'partial', 'whats-new' ); ?>

<?php
				if ( have_posts() ) while ( have_posts() ) :
					the_post();
					$category = get_the_category(); 
					$cat_link = get_category_link($category[0]->cat_ID);
?>
				<section class="blog-content">
					<div class="container">
						<div class="article-wrapper">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<h1 class="entry-title"><?php the_title(); ?></h1>
								<div class="entry-meta">
									<span class="post-by">Posted by <?php the_author(); ?></span>
									<span class="post-date">on <?php the_date(); ?></span>,
									<a class="post-category" href="<?php echo $cat_link;?>"><?php echo $category[0]->name; ?></a>
								</div><!-- .entry-meta -->
								
								<div class="entry-content">
									<?php the_content(); ?>
									<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
								</div><!-- .entry-content -->

								<footer class="entry-utility">
									<button class="share-toggle">Share</button>
									<div class="share">
										<?php echo do_shortcode( "[wp_social_sharing social_options='facebook,twitter,googleplus,linkedin' facebook_text='Share on Facebook' twitter_text='Share on Twitter' googleplus_text='Share on Google+' linkedin_text='Share on Linkedin' icon_order='f,t,g,l' show_icons='1']" ); ?>
									</div>
								</footer><!-- .entry-utility -->
							</article><!-- #post-## -->
							
							<nav id="nav-below" class="navigation button-container">
								<div class="nav-previous"><?php previous_post_link( '%link', 'Previous' ); ?></div>
								<div class="nav-next"><?php next_post_link( '%link', 'Next' ); ?></div>
							</nav><!-- #nav-below -->
							
							<?php disqus_embed('emergingmedia-dev'); ?>
						</div><!-- .article-wrapper -->

						<div class="sidebar-wrapper">
							<?php get_sidebar(); ?>
						</div>
					</div><!-- .container -->
				</section>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
