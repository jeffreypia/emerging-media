<?php
	// This partial pulls the most latest post.
	// Used query_posts due to requirement for pagination
	// $latestPost = query_posts('posts_per_page=1&post_type=post');

	// if($latestPost) :
	// 	foreach ($latestPost as $post) :
	// 		setup_postdata($post);
?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<div class="entry-meta">
			<span class="post-by">Posted by <?php the_author(); ?></span>
			<span class="post-date">on <?php the_date(); ?></span>,
			<a class="post-category" href="<?php echo $cat_link;?>"><?php echo $category[0]->name; ?></a>
		</div><!-- .entry-meta -->
		
		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
		</div><!-- .entry-content -->

		<footer class="entry-utility">
			<button class="share-toggle">Share</button>
			<div class="share">
				<?php echo do_shortcode( "[wp_social_sharing social_options='facebook,twitter,googleplus,linkedin' facebook_text='Share on Facebook' twitter_text='Share on Twitter' googleplus_text='Share on Google+' linkedin_text='Share on Linkedin' icon_order='f,t,g,l' show_icons='1']" ); ?>
			</div>
		</footer><!-- .entry-utility -->
	</article><!-- #post-## -->
	<?php endwhile; ?>

	<nav id="nav-below" class="navigation button-container">
		<div class="nav-previous"><?php previous_post_link( '%link', 'Previous' ); ?></div>
		<div class="nav-next"><?php next_post_link( '%link', 'Next' ); ?></div>
	</nav><!-- #nav-below -->

<?php
			// global $withcomments; $withcomments = 1;
			disqus_embed('emergingmedia-dev');
	// 	endforeach;
	// endif;
	// wp_reset_query();
	endif;
	wp_reset_postdata();
?>
	