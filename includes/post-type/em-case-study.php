<?php

add_action( 'init', 'register_cpt_case_study' );

function register_cpt_case_study() {

    $labels = array( 
        'name' => _x( 'Case Study', 'em_case_study' ),
        'singular_name' => _x( 'Case Study', 'em_case_study' ),
        'add_new' => _x( 'Add New Case Study', 'em_case_study' ),
        'all_items' => _x( 'Case Studies', 'em_case_study' ),
        'add_new_item' => _x( 'Add New Case Study', 'em_case_study' ),
        'edit_item' => _x( 'Edit Case Study', 'em_case_study' ),
        'new_item' => _x( 'New Case Study', 'em_case_study' ),
        'view_item' => _x( 'View Case Study', 'em_case_study' ),
        'search_items' => _x( 'Search Case Studies', 'em_case_study' ),
        'not_found' => _x( 'No Case Studies found', 'em_case_study' ),
        'not_found_in_trash' => _x( 'No Case Studies found in Trash', 'em_case_study' ),
        'parent_item_colon' => _x( 'Parent Case Study:', 'em_case_study' ),
        'menu_name' => _x( 'Case Study', 'em_case_study' )
    );

    $supports = array(
        'title',
        'page-attributes',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'case-study' ),
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => false,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-analytics',
    );

    register_post_type( 'em_case_study', $args );
}