<?php

add_action( 'init', 'register_cpt_slide' );

function register_cpt_slide() {

    $labels = array( 
        'name' => _x( 'Slide', 'em_slide' ),
        'singular_name' => _x( 'Slide', 'em_slide' ),
        'add_new' => _x( 'Add New Slide', 'em_slide' ),
        'all_items' => _x( 'Slides', 'em_slide' ),
        'add_new_item' => _x( 'Add New Slide', 'em_slide' ),
        'edit_item' => _x( 'Edit Slide', 'em_slide' ),
        'new_item' => _x( 'New Slide', 'em_slide' ),
        'view_item' => _x( 'View Slide', 'em_slide' ),
        'search_items' => _x( 'Search Slides', 'em_slide' ),
        'not_found' => _x( 'No Slides found', 'em_slide' ),
        'not_found_in_trash' => _x( 'No Slides found in Trash', 'em_slide' ),
        'parent_item_colon' => _x( 'Parent Slide:', 'em_slide' ),
        'menu_name' => _x( 'Slide', 'em_slide' )
    );

    $supports = array(
        'title',
        'editor',
        'thumbnail',
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-images-alt',
    );

    register_post_type( 'em_slide', $args );
}