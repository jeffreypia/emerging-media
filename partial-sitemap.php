<section class="site-map">
	<div class="container">
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'sitemap-1' ) ); ?>
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'sitemap-2' ) ); ?>
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'sitemap-3' ) ); ?>
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'sitemap-4' ) ); ?>
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'sitemap-5' ) ); ?>
	</div><!-- .container -->
</section>