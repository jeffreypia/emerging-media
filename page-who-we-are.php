<?php
/**
 * The Who We Are page template.
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
	<?php get_template_part( 'banner', 'who-we-are' ); ?>

	<?php get_template_part( 'partial', 'bio' ); ?>

	<?php get_template_part( 'partial', 'endorsement' ); ?>

	<?php get_template_part( 'partial', 'people' ); ?>

	<?php get_template_part( 'partial', 'apply' ); ?>

	<?php get_template_part( 'partial', 'connections' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>