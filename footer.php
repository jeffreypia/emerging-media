<?php
/**
 * The template for displaying the footer.
 */
?>
		</section><!-- #main -->
		<footer role="contentinfo" class="site-footer">
			<div class="container">
				<?php wp_nav_menu( array( 'container_class' => 'primary', 'theme_location' => 'primary' ) ); ?>
				<?php wp_nav_menu( array( 'container_class' => 'social', 'theme_location' => 'social' ) ); ?>
			</div><!-- .footer-wrapper -->
		</footer><!-- footer -->
<?php wp_footer(); ?>
	</body>
</html>