<li class="slide slide-<?php echo $post->post_name; ?> <?php if( get_field('skrim') ) { echo 'skrim'; } ?>">
	<div class="container">
		<header class="slide-header">
			<h3 class="slide-title"><?php echo get_field('title') ?></h3>
		</header>

		<article class="slide-content">
			<?php the_content(); ?>
		</article>
<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('full', array( 'class'	=> 'slide-bg'));
		} 
?>
	</div><!-- .container -->
</li>
