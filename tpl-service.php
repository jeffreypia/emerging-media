<?php
/**
 * Template Name: Service page
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php $img = get_field('banner_image'); ?>
				<section class="banner service-banner" style="background-image: url(<?php echo $img['url']; ?>);">
					<div class="container">
						<h1 class="section-title"><?php the_field('banner_title'); ?></h1>
					</div>
				</section>

				<section class="service-callout">
					<div class="container">
						<h2 class="section-title"><?php the_field('banner_subtitle'); ?></h2>
						<article class="section-content">
							<?php the_field('banner_copy'); ?>
						</article>
					</div>
				</section>

				<?php if(get_field('service')) : ?>
				<section class="service-details">
					<div class="container">
						<ul class="service-details-controls count-<?php echo count( get_field('service') ); ?>">
							<?php
								$i = 0;
								while(has_sub_field('service')) :
							?>
									<li class="service-details-control">
										<button class="button <?php if($i === 0) { echo 'active'; } ?>" data-index="<?php echo $i; ?>"><?php the_sub_field('service_name'); ?></button>
									</li>
							<?php
									$i++;
								endwhile;
							?>
						</ul>
						<ul class="service-details-contents">
							<?php while(has_sub_field('service')) : ?>
								<li class="service-details-content">
									<h3 class="section-title"><?php the_sub_field('service_name'); ?></h3>
									<article class="section-content">
										<?php the_sub_field('service_details'); ?>
									</article>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
					
				</section>
				<?php endif; ?>


				<?php get_template_part( 'partial', 'services' ); ?>
<?php endwhile; ?>
<?php get_footer(); ?>