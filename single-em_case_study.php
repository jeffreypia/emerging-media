<?php
/**
 * Template Name: Case Study
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php $img = get_field('banner_image'); ?>
				<section class="banner cs-banner" style="background-image: url(<?php echo $img['url']; ?>);">
					<div class="container">
						<?php $img = get_field('cs-logo-white'); ?>
						<div class="logo-wrapper">
							<img class="cs-logo" src="<?php echo $img['url']; ?>" title="<?php echo the_title_attribute(); ?>">
						</div>
						<h1 class="section-title"><?php the_field('banner_title'); ?></h1>
					</div>
				</section>

				<section class="cs-challenge">
					<div class="container">
						<h2 class="section-title">The Challenge</h2>
						<article class="section-content">
							<?php the_field('cs-challenge'); ?>
						</article>
					</div>
				</section>

				<section class="cs-cta cs-cta-1">
					<div class="container">
<?php
						if( get_field('cs-cta_1_image') ) :
							$img = get_field('cs-cta_1_image');
?>
							<div class="logo-wrapper">
								<img class="cta-img" src="<?php echo $img['url']; ?>">
							</div>
						<?php endif; ?>
						<article class="section-content">
							<?php the_field('cs-cta_1_copy'); ?>
						</article>
					</div>
				</section>

				<section class="cs-details">
<?php
				if( has_post_thumbnail() ) :
					$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
?>
					<img class="cs-featured-image" src="<?php echo $img[0]; ?>">
				<?php endif; ?>
					<div class="container">
						<ul class="cs-details-controls">
							<li class="cs-details-control">
								<button class="button active" data-index="0">Results</button>
							</li>
							<li class="cs-details-control">
								<button class="button" data-index="1">Strategy</button>
							</li>
						</ul>
						<ul class="cs-details-contents">
							<li class="cs-details-content">
								<h3 class="section-title">Results</h3>
								<article class="section-content">
									<?php the_field('cs-results'); ?>
								</article>
							</li>
							<li class="cs-details-content">
								<h3 class="section-title">Strategy</h3>
								<article class="section-content">
									<?php the_field('cs-strategy'); ?>
								</article>
							</li>
						</ul>
					</div>
				</section>

				<section class="cs-cta cs-cta-2">
					<div class="container">
						<article class="section-content">
							<?php the_field('cs-cta_2_copy'); ?>
						</article>
<?php
						if( get_field('cs-cta_2_image') ) :
							$img = get_field('cs-cta_2_image');
?>
							<div class="logo-wrapper">
								<img class="cta-img" src="<?php echo $img['url']; ?>">
							</div>
						<?php endif; ?>
					</div>
				</section>

				<section class="cs-menu">
					<div class="container">
						<h3 class="section-title">Case Studies</h3>

						<nav class="cs-nav">
							<ul class="slides">
								<li class="slide">
									<a <?php if( $post->post_name === 'toddy' ) { echo 'class="active"'; } ?> href="/case-study/toddy/" title="Toddy"><img src="/wp-content/uploads/slide-logo-toddy.png"></a>
								</li>
								<li class="slide">
									<a <?php if( $post->post_name === 'atrium' ) { echo 'class="active"'; } ?> href="/case-study/atrium/" title="Atrium"><img src="/wp-content/uploads/slide-logo-atrium.png"></a>
								</li>
								<li class="slide">
									<a <?php if( $post->post_name === 'equitrac' ) { echo 'class="active"'; } ?> href="/case-study/equitrac/" title="Equitrac"><img src="/wp-content/uploads/slide-logo-equitrac.png"></a>
								</li>
								<li class="slide">
									<a <?php if( $post->post_name === 'liazon' ) { echo 'class="active"'; } ?> href="/case-study/liazon/" title="Liazon"><img src="/wp-content/uploads/slide-logo-liazon.png"></a>
								</li>
								<li class="slide">
									<a <?php if( $post->post_name === 'the-company-store' ) { echo 'class="active"'; } ?> href="/case-study/the-company-store/" title="The Company Store"><img src="/wp-content/uploads/slide-logo-company-store.png"></a>
								</li>
								<li class="slide">
									<a <?php if( $post->post_name === 'xmpie' ) { echo 'class="active"'; } ?> href="/case-study/xmpie/" title="XMPIE"><img src="/wp-content/uploads/slide-logo-xmpie.png"></a>
								</li>
							</ul>
						</nav>
					</div>
				</section>

				<?php // get_template_part( 'partial', 'services' ); ?>
<?php endwhile; ?>
<?php get_footer(); ?>