<?php
/**
 * The home page template.
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'banner', 'home' ); ?> 

	<?php get_template_part( 'partial', 'services' ); ?> 

	<?php get_template_part( 'partial', 'journey' ); ?>

	<?php get_template_part( 'slider', 'home' ); ?>

	<?php get_template_part( 'partial', 'awards' ); ?>

	<?php get_template_part( 'partial', 'sitemap' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>