<?php
	$logo = get_field('logo');
	$target = get_field('link');
?>
<li class="slide slide-case-study slide-<?php echo $post->post_name; ?> <?php if( get_field('skrim') ) { echo 'skrim'; } ?>">
	<div class="container">
		<header class="slide-header">
			<?php if( get_field('logo_position') ) : ?>
				<h3 class="slide-title"><?php echo get_field('title') ?></h3>
				<img class="slide-logo" src="<?php echo $logo['url']; ?>" alt="">
			<?php else : ?>
				<img class="slide-logo" src="<?php echo $logo['url']; ?>" alt="">
				<h3 class="slide-title"><?php echo get_field('title') ?></h3>
			<?php endif; ?>
		</header>

		<article class="slide-content">
			<?php the_content(); ?>
		</article>

		<div class="button-container">
			<a class="button" href="<?php echo get_permalink( $target->ID ); ?>" title="Link to <?php echo $target->post_title; ?>">View Case Study</a>
		</div><!-- .button-container -->
<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('full', array( 'class'	=> 'slide-bg'));
		} 
?>
	</div><!-- .container -->
</li>
