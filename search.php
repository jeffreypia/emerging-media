<?php
/**
 * The search results template file.
 */

get_header(); ?>

				<section class="banner blog-banner">
					<div class="container">
						<h1 class="section-title">Content that Matters</h1>
					</div>
				</section>

				<?php get_template_part( 'partial', 'whats-new' ); ?>

				<section class="blog-content">
					<div class="container">
						<div class="article-wrapper">
							<?php get_template_part( 'partial', 'latest-post' ); ?>
						</div><!-- .article-wrapper -->

						<div class="sidebar-wrapper">
							<?php get_template_part( 'partial', 'category-filter' ); ?>

							<?php get_search_form(); ?>
							
							<?php get_template_part( 'loop', 'search' ); ?>
						</div><!-- sidebar-wrapper -->
					</div><!-- .container -->
				</section>

<?php get_footer(); ?>
