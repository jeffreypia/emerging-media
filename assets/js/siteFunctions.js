function isElementInViewport (el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

$('html').removeClass('no-js').addClass('js');

$('.see-more').on('click', function() {
	$(this).hide().parent().find('.hidden').hide().removeClass('hidden').slideDown();
});

$('.service-details-control').on('click', '.button', function(e) {
	var $this = $(this),
		targetIndex = $this.data('index'),
		$target;

	if(!$this.hasClass('active')) {
		targetIndex = $this.data('index'),
		$target = $this.closest('.service-details').find('.service-details-content').eq( targetIndex );

		$this.addClass('active').parent().siblings().find('.active').removeClass('active');
		$target.fadeIn().siblings().hide();

		if( !isElementInViewport( $target ) ) {
			$('html, body').animate({
				scrollTop: ($target.offset().top - 100)
			}, 500);
		}
	}
});

$('.cs-details-control').on('click', '.button', function(e) {
	var $this = $(this);
	if(!$this.hasClass('active')) {
		$this.addClass('active').parent().siblings().find('.active').removeClass('active').closest('.cs-details').find('.cs-details-content').eq($(this).data('index')).fadeIn().siblings().hide()
	}
});

$('.share-toggle').on('click', function(){
	$(this).toggleClass("expanded");
});

$('.slider-main').flexslider({
	animation: 'slide',
	controlNav: false,
	smoothHeight: true
});

$('.cs-nav').flexslider({
	animation: 'slide',
    animationLoop: false,
	controlNav: false,
    itemWidth: 150,
    slideshow: false
});

$('.awards-list').flexslider({
	animation: 'slide',
    animationLoop: false,
	controlNav: false,
    itemWidth: 196,
    slideshow: false
});

$('.person').on({
	click: function() {
		var $this = $(this);

		if($this.hasClass('expanded')) {
			$this.removeClass("expanded");
		} else {
			$this.addClass("expanded").siblings().removeClass("expanded");
		}
	},
	mouseenter: function() {
		$(this).addClass("expanded").siblings().removeClass("expanded");
	},
	mouseleave: function() {
		$(this).removeClass("expanded");
	}
});