<?php
	$logo = get_field('logo');
?>
<li class="slide slide-testimonial slide-<?php echo $post->post_name; ?>">
	<div class="container">
		<article class="slide-content">
			<?php the_content(); ?>

			<footer class="testimonial-footer">
				<strong class="testmonial-attribution">– <?php echo get_field('attribution'); ?></strong>
				<img class="slide-logo" src="<?php echo $logo['url']; ?>" alt="">
			</footer>
		</article>
	</div>
</li>
