<?php
/**
 * Page of slides
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<section class="slider slider-main">
					<ul class="slides">
						<?php get_template_part( 'slide', 'equitrac' ); ?>

						<?php get_template_part( 'slide', 'tcs' ); ?>

						<?php get_template_part( 'slide', 'toddy' ); ?>

						<?php get_template_part( 'slide', 'liazon' ); ?>

						<?php get_template_part( 'slide', 'xmpie' ); ?>

						<?php get_template_part( 'slide', 'atrium' ); ?>

						<?php get_template_part( 'slide', 'blog-1' ); ?>

						<?php get_template_part( 'slide', 'blog-2' ); ?>

						<?php get_template_part( 'slide', 'blog-3' ); ?>

						<?php get_template_part( 'slide', 'blog-4' ); ?>

						<?php get_template_part( 'slide', 'blog-5' ); ?>

						<?php get_template_part( 'slide', 'testimonial-1' ); ?>

						<?php get_template_part( 'slide', 'testimonial-2' ); ?>

						<?php get_template_part( 'slide', 'testimonial-3' ); ?>

						<?php get_template_part( 'slide', 'testimonial-4' ); ?>

						<?php get_template_part( 'slide', 'testimonial-5' ); ?>

					</ul>
				</section>
<?php endwhile; ?>
<?php get_footer(); ?>