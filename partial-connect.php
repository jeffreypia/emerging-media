<?php $img = get_field('connect_section_image'); ?>
<section class="connect">
	<img src="<?php echo $img['url']; ?>" alt="Map">
	<h1 class="section-title"><?php echo get_field('connect_section_title'); ?></h1>
	<article class="section-copy"><?php echo get_field('connect_section_copy'); ?></article>
</section>
