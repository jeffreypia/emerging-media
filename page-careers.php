<?php
/**
 * The Our Work page template.
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'banner', 'our-work' ); ?>

				<?php get_template_part( 'partial', 'our-work' ); ?>

				<?php get_template_part( 'slider', 'careers' ); ?>

				<?php get_template_part( 'partial', 'values' ); ?>

				<?php get_template_part( 'partial', 'hireology' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>