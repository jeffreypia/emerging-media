<?php
	global $post;
	$services = get_pages(array(
		'sort_column' 	=> 'menu_order',
		'meta_key' 		=> '_wp_page_template',
		'meta_value' 	=> 'tpl-service.php',
		'hierarchical' 	=> 0,
	));
?>
<section class="services">
	<ul>
<?php
		foreach($services as $post) :
			setup_postdata( $post );
			$img = get_field('thumbnail');
?>
			<li class="service <?php echo $post->post_name; ?>">
				<a href="<?php the_permalink(); ?>">
					<img class="service-image" src="<?php echo $img['url']; ?>" alt="<?php echo the_title_attribute(); ?>" />
					<h3 class="service-title"><?php echo get_field('title'); ?></h3>
					<article class="service-copy">
						<p><?php echo get_field('copy'); ?></p>
					</article>
				</a>
			</li>
<?php
		endforeach;
		wp_reset_postdata();
?>
	</ul>
</section>