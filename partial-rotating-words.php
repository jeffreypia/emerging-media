				<section class="rw-wrapper">
					<div class="rw-sentence">
						<span class="rw-intro">You disrupt we</span>
						<div class="rw-words rw-words-1">
							<span><strong>build credibility</strong>.</span>
							<span><strong>innovate</strong>.</span>
							<span><strong>create</strong>.</span>
							<span><strong>tell stories</strong>.</span>
							<span><strong>strategize</strong>.</span>
							<span><strong>engage</strong>.</span>
							<span><strong>captivate</strong>.</span>
							<span><strong>compel</strong>.</span>
							<span><strong>build relationships</strong>.</span>
							<span><strong>explore</strong>.</span>
							<span><strong>target</strong>.</span>
							<span><strong>share</strong>.</span>
							<span><strong>cultivate</strong>.</span>
							<span><strong>position</strong>.</span>
							<span><strong>analyze</strong>.</span>
							<span><strong>influence</strong>.</span>
							<span><strong>connect</strong>.</span>
						</div>
					</div>
				</section>
