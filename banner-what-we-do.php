<?php $bg = get_field('banner_image'); ?>
<section id="banner" class="banner what-we-do" <?php if( $bg ) { echo 'style="background-image: url('.$bg['url'].');"'; } ?>>
	<div class="container">
		<h1 class="section-title"><?php echo get_field('banner_title'); ?></h1>
		<h2 class="section-subtitle"><?php echo get_field('banner_subtitle'); ?></h2>
		<article class="section-copy"><?php echo get_field('banner_copy'); ?></article>
	</div><!-- .container -->
</section>
