<?php
/**
 * The Connect page template.
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'partial', 'contact' ); ?>

				<?php get_template_part( 'partial', 'apply' ); ?>

				<?php get_template_part( 'partial', 'connect' ); ?>

<?php endwhile; ?>
<?php get_footer(); ?>