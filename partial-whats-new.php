<section class="recent-posts">
	<?php
		$args = array(
			'numberposts' => 3,
		);

		$recent_posts = wp_get_recent_posts( $args );
	?>
	<div class="container">
		<h1 class="section-title">What's New?</h1>
		<ul class="posts">
<?php
			foreach( $recent_posts as $recent ) :
				$category = get_the_category($recent[ID]); 
?>
				<li class="post cat-<?php echo $category[0]->slug ?>">
					<h3 class="post-title"><?php echo $recent[post_title]; ?></h3>

					<div class="post-meta">
						<span class="post-date">Posted on <?php echo get_the_date( 'F j, Y', $recent[ID] ); ?></span>
						<span class="post-comment-count disqus-comment-count" data-disqus-url="<?php echo get_permalink( $recent[ID] ); ?>"><?php echo $recent[comment_count] ?> comments</span>
					</div>

					<div class="button-container">
						<a class="button" href="<?php echo get_permalink( $recent[ID] ); ?>">Read More</a>
					</div>
				</li>
<?php
			endforeach;
?>
			</ul>
	</div>
</section>
