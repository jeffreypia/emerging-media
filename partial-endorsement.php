<?php $img = get_field('testimonial_attribution_logo'); ?>
<section class="endorsement">
	<div class="container">
		<?php echo get_field('testimonial_copy'); ?>
		<div class="endorsement-name">- <?php echo get_field('testimonial_attribution'); ?></div>
		<img class="endorsement-logo" src="<?php echo $img['url']; ?>" alt="">
	</div>
</section>
