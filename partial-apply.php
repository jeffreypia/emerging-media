<section class="apply">
	<div class="container">
		<p><?php echo get_field('apply_cta_copy'); ?></p>
		<a class="button alt-2" href="<?php echo get_field('apply_cta_link') ?>"><?php echo get_field('apply_cta_link_copy') ?></a>
	</div>
</section>
